"use strict"
// 1. Запитайте у користувача два числа. Перевірте, чи є кожне з введених значень числом. Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом. Виведіть на екран всі числа від меншого до більшого за допомогою циклу for.


let number1 = prompt("Введите любое число!");
while (isNaN(number1)) {
  alert("Введите число!");
  number1 = prompt("Введите любое число!");
}

let number2 = prompt("Введите еще одно число!");
while (isNaN(number2)) {
    alert("Введите число!");
    number2 = prompt("Введите еще одно число!");
}

for (; ;) {
    if (number1 >= number2) {
        console.log(number2);
        console.log(number1);
        break;
    } else if (number1 < number2) {
        console.log(number1);
        console.log(number2);
        break;
    }
}

// 2. Напишіть програму, яка запитує в користувача число та перевіряє, чи воно є парним числом. Якщо введене значення не є парним числом, то запитуйте число доки користувач не введе правильне значення.

let isDoubleNumber = prompt("Введите парное число!");

while (isDoubleNumber % 2 !== 0) {
    alert(" Введите парное число!")
    isDoubleNumber = prompt("Введите парное число!");
}

